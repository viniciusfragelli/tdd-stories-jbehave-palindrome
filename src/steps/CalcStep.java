package steps;

import junit.framework.Assert;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

public class CalcStep {

	String palavra;
	boolean teste;
	
	@Given("we type a <string>")
	public void debugStepGiven(@Named("string") String p)
	{
		palavra = p;
	}
	
	@When("we ask Palindromaton to verify it")
	public void debugStepWhen()
	{
		String aux = "";
		palavra = palavra.replaceAll(" ", "");
		for(int i=palavra.length() - 1;i>-1;i--){
			aux += palavra.charAt(i);
		}
		System.out.println(aux);
		if(aux.equalsIgnoreCase(palavra)){
			teste = true;
		}else{
			teste = false;
		}
	}
	
	@Then("we should be notified if it <isPalindrome>")
	public void debugStepThen(@Named("isPalindrome") boolean result)
	{
		Assert.assertEquals(result, teste);
	}
}
