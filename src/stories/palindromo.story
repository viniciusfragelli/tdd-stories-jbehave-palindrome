Given we type a <string>
When we ask Palindromaton to verify it
Then we should be notified if it <isPalindrome>

Examples:
| string                        | isPalindrome |
| a                             |        true  |  
| aa                            |        true  |
| pop                           |         true |
| Joaquim                       |        false |  
| Malayalam                     |         true |  
| Amma                          |         true |  
| Madam                         |         true |  
| Sator Arepo Tenet Opera Rotas |         true |
| A base do teto desaba			|         true |
| Anotaram a data da maratona   |         true |
| 121                           |         true |
| 112211                        |         true |
| não é palindromo              |        false |
| 23/1/41                       |        false |
| 21/8/12                       |         true |